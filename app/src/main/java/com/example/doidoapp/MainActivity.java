package com.example.doidoapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> items;
    private ArrayList<String> doneitems;
    private ArrayAdapter<String> itemsAdapter;
    private ListView listView;
    private Button button;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        TinyDB tinydb = new TinyDB(this);

        SharedPreferences sharedPreferences = getSharedPreferences("sharedPrefs", MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        final boolean isDarkModeOn = sharedPreferences.getBoolean("isDarkModeOn", false);

        switch(item.getItemId()) {
            case R.id.action_done:
                tinydb.putListString("todos", items);
                tinydb.putListString("done", doneitems);
                Intent intent = new Intent(this, Done.class);
                startActivity(intent);
                return true;

            case R.id.action_reset:
                Toast.makeText(getApplicationContext(), "Clean", Toast.LENGTH_LONG).show();
                items.clear();
                doneitems.clear();
                tinydb.putListString("todos", items);
                tinydb.putListString("done", doneitems);
                itemsAdapter.notifyDataSetChanged();
                return true;

            case R.id.action_layout:

                if (isDarkModeOn) {
                    // if dark mode is on it
                    // will turn it off
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    // it will set isDarkModeOn
                    // boolean to false
                    editor.putBoolean("isDarkModeOn", false);
                }
                else {
                    // if dark mode is off
                    // it will turn it on
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

                    // it will set isDarkModeOn
                    // boolean to true
                    editor.putBoolean("isDarkModeOn", true);
                }
                editor.apply();


            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Saving state of our app
        // using SharedPreferences
        SharedPreferences sharedPreferences = getSharedPreferences("sharedPrefs", MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        final boolean isDarkModeOn = sharedPreferences.getBoolean("isDarkModeOn", false);

        // When user reopens the app
        // after applying dark/light mode
        if (isDarkModeOn) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }

        else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        listView = findViewById(R.id.listView);
        button = findViewById(R.id.button);

        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        final MediaPlayer mp = MediaPlayer.create(this, R.raw.click);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addItem(view);
                mp.start();
            }
        });

        items = new ArrayList<>();
        doneitems = new ArrayList<>();

        TinyDB tinydb = new TinyDB(this);
        doneitems.addAll(tinydb.getListString("done"));

        itemsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_single_choice, items);
        listView.setAdapter(itemsAdapter);

        moveItem();
        removeItem();
    }

    private void moveItem() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Context context = getApplicationContext();
                Toast.makeText(context, "Done", Toast.LENGTH_LONG).show();

                listView.setItemChecked(position, false);
                doneitems.add(items.get(position));
                items.remove(position);
                itemsAdapter.notifyDataSetChanged();
            }
        });
    }

    private void removeItem() {
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Context context = getApplicationContext();
                Toast.makeText(context, "Deleted", Toast.LENGTH_LONG).show();

                listView.setItemChecked(position, false);

                items.remove(position);
                itemsAdapter.notifyDataSetChanged();
                return true;
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        TinyDB tinydb = new TinyDB(this);
        //Collections.reverse(items);
        tinydb.putListString("todos", items);
        items.clear();
    }

    @Override
    protected void onResume() {
        TinyDB tinydb = new TinyDB(this);
        super.onResume();
        items.clear();
        items.addAll(tinydb.getListString("todos"));
        //itemsAdapter.notifyDataSetChanged();
    }

    private void addItem(View view){
        TinyDB tinydb = new TinyDB(this);
        EditText input = findViewById(R.id.textInput);
        //input = findViewById(R.id.textInput);
        String itemText = input.getText().toString();

        if (!(itemText.equals(""))){
            Collections.reverse(items);
            items.add(itemText);
            Collections.reverse(items);
            itemsAdapter.notifyDataSetChanged();
            tinydb.putListString("todos", items);

            input.setText("");
        }
        else{
            Toast.makeText(getApplicationContext(), "Bitte gib ein ToDo ein!", Toast.LENGTH_LONG).show();
        }

    }
}

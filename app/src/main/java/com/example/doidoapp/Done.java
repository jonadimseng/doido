package com.example.doidoapp;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Collections;

public class Done extends AppCompatActivity {

    private ListView listView;
    private ArrayList<String> items;
    private ArrayList<String> doneitems;
    private ArrayAdapter<String> itemsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TinyDB tinydb = new TinyDB(this);
        items = new ArrayList<>();
        items.addAll(tinydb.getListString("todos"));
        doneitems = new ArrayList<>();
        itemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_checked, doneitems);
        listView = findViewById(R.id.donelist);
        listView.setChoiceMode(ListView.CHOICE_MODE_NONE);
        listView.setAdapter(itemsAdapter);
        doneitems.addAll(tinydb.getListString("done"));

        removeItem();
        moveItem();
    }

    private void moveItem() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Context context = getApplicationContext();
                TinyDB tinydb = new TinyDB(getApplicationContext());
                Toast.makeText(context, "Moved back", Toast.LENGTH_LONG).show();

                listView.setItemChecked(position, false);
                Collections.reverse(items);
                items.add(doneitems.get(position));
                Collections.reverse(items);
                tinydb.putListString("todos", items);
                doneitems.remove(position);
                itemsAdapter.notifyDataSetChanged();
            }
        });
    }

    private void removeItem() {
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Context context = getApplicationContext();
                Toast.makeText(context, "Deleted", Toast.LENGTH_LONG).show();
                listView.setItemChecked(position, false);
                doneitems.remove(position);
                itemsAdapter.notifyDataSetChanged();
                return true;
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        TinyDB tinydb = new TinyDB(this);
        tinydb.putListString("done", doneitems);
        doneitems.clear();
    }

    @Override
    protected void onResume() {
        TinyDB tinydb = new TinyDB(this);
        super.onResume();
        doneitems.clear();
        doneitems.addAll(tinydb.getListString("done"));
    }
}

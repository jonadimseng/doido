# Doido

Doido helps to make your everyday life easier. Alternative To Do's, extra functions and features are combined here in one package. It is a simple to-do app.

## Features

- Save to-do's
- Completed to-do's are moved to a done-List
- Delete entries from to-do's or done-List
- Reset to-do's or done-List
- Light/dark mode toggle
## Built With

* [Java](https://www.java.com/en/)
* [Android Studio](https://developer.android.com/studio/)
* [TinyDB](https://github.com/kcochibili/TinyDB--Android-Shared-Preferences-Turbo)


## Screenshots

<img src="https://i.imgur.com/R49EdYE.png" alt="todo-list" width="200"/>
<img src="https://i.imgur.com/RfVPtqK.png" alt="done-list" width="200"/>

## Acknowledgements

* [FH-Campus Wien](https://www.fh-campuswien.ac.at/)
* [Stackoverflow](https://stackoverflow.com/)
* [Android Documentation](https://developer.android.com/docs/)
